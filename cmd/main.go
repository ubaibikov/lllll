package main

import (
	"fmt"
	"net/http"
	"service"
	"service/transport"
	http2 "service/transport/http"
)

func main() {

	var svc = service.Upp{}
	points := transport.MakeEndpoints(svc)
	r := http2.NewService(points)

	http.Handle("/", r)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Printf("error is %v", err)
	}

}