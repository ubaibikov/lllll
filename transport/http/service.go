package http

import (
	"context"
	"encoding/json"
	transport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
	transporthttp "service/transport"
)

func NewService(e transporthttp.Endpoints) http.Handler {
	var (
		r   = mux.NewRouter()
	)

	r.Methods("POST").Path("/uppercase").Handler(transport.NewServer(
		e.Upper,
		decodeUppercaseRequest,
		encodeResponse,
	))

	return r
}

func decodeUppercaseRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request transporthttp.UppercaseRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}