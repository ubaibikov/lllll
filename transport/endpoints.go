package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"service"
)


type Endpoints struct {
	Upper endpoint.Endpoint
}


func MakeEndpoints(s service.StringService) Endpoints  {
	return Endpoints{
		Upper: makeUppercaseEndpoint(s),
	}
}

func makeUppercaseEndpoint(svc service.StringService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(UppercaseRequest)
		v, err := svc.Uppercase(req.S)
		if err != nil {
			return UppercaseResponse{v, err.Error()}, nil
		}
		return UppercaseResponse{v, ""}, nil
	}
}